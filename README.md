# Storage service carrierwave engine integration gem

This gem act as glue between [storage service client](https://bitbucket.org/trileuco/triskel-storage-service-ruby-client) and [carrierwave](https://github.com/carrierwaveuploader/carrierwave) ruby lib.
To import this gem in your project, just add it in your Gemfile. Your build host must be SSH authenticated with repos.
```ruby
gem 'triskel_storage_service_carrierwave',
 git: 'git@bitbucket.org:trileuco/triskel-storage-service-carrierwave.git', tag: 'vthe.last.tag', submodules: true
```
You will need to require carrierwave in your application by your own.

## Rails application integration

#### Prepare your settings file

We need to include two URLS.
* Internal. For the communication between the client application (Rails) and the storage service.
* External. This is the url that will be placed on templates for the browsers to reach attachments.  

```yaml
external_services:
  storage:
    internal_base_url: "http://localhost:8080"
    external_base_url: "http://localhost:8080"
```

#### Adding storage service engine configs to carrierwave at runtime (Rails)
In Rails, enable the new storage engine accessible urls configs on carrierwave initializer, on this example, config/initializers/carrierwave.rb
```ruby
CarrierWave.configure do |config|
  config.storage = :triskel
  config.asset_host = Settings.external_services.storage.external_base_url
  config.triskel_base_url = Settings.external_services.storage.internal_base_url
end
```

If you want, you could decide in this config use different storages depending of the Rails environment, for example:
```ruby
CarrierWave.configure do |config|
  if Rails.env.test?
    config.storage = :file
    config.asset_host = ActionController::Base.asset_host
    config.store_dir = 'uploads'
  else
    config.storage = :triskel
    config.asset_host = Settings.external_services.storage.external_base_url
    config.triskel_base_url = Settings.external_services.storage.internal_base_url
  end
end
```

#### Creating a parent class for your uploaders
You can create a parent class that your uploaders will subclass.
You can also override here al methods you want and then make your uploaders a subclass of this one.

```ruby
class TriskelUploader < CarrierWave::Uploader::Base
  def cache_control
    'public, max-age=31536000'
  end
end
```

To use this gem with the Triskel Storage Service properly, you need use unique filenames for each file uploaded to the storage, so we must follow the instructions included in the following page of the carrierwave wiki: [How to: Create random and unique filenames for all versioned files
](https://github.com/carrierwaveuploader/carrierwave/wiki/how-to:-create-random-and-unique-filenames-for-all-versioned-files)

So, we recommend you resolve that in your base uploader as follow:

```ruby
class TriskelUploader < CarrierWave::Uploader::Base
  ...

  def filename
    return if original_filename.blank?

    CarrierWave::Storage::TriskelUUIDGenerator.gen(model.class.name.underscore, mounted_as, secure_token)
  end

  protected

  def secure_token
    var = :"@#{mounted_as}_secure_token"
    model.instance_variable_get(var) || model.instance_variable_set(var, SecureRandom.uuid)
  end
end
```

Currently, the latest version of the Triskel Storage Service (the v1.3.0), doesn't support download files with '/' characters in its uuid, so you should be careful not to include that character on the result of the `filename` method neither set a diferent value than `nil` to the `config.store_dir` param (both are joined with '/' to build the file uuid). You can watch the Triskel Storage Service changelog, to see if this behaviour is changed in future versions and, in that case, you will be able to use '/' characters in the `filename` method or set a value of the `config.store_dir`.

Later, we can subclass our new uploader:
```ruby
class LogoUploader < TriskelUploader
  def labels
    %w[app logo]
  end

  def extension_white_list
    %w[jpg jpeg png]
  end
end
```  
Labels will be added to the final destination at storage service.

## How to run tests

In the root of the project, type the following
```bash
rake test_units
```

## Code syntax and conventions checks

We use overcommit for automated git hooks that validate code syntax and conventions. To configure overcommit go to the project root and execute:

```bash
bundle install --gemfile=.overcommit_gems.rb
overcommit --install
```

Check that everything is working correctly with:

```bash
overcommit -r
```

Now before each commit the validations will be executed and if something isn't correct the commit won't be done.
