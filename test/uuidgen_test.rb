# frozen_string_literal: true

require 'test/unit'
require_relative '../lib/uuid'

module CarrierWave
  module Storage
    class TriskelUUIDGenTest < Test::Unit::TestCase
      def test_uuid_gen
        samples = gen_uuid_samples
        samples.each do |c|
          result = CarrierWave::Storage::TriskelUUIDGenerator.gen(c['model_class'], c['mounted_as'], c['cache_buster'])
          assert_equal(c['expected'], result, "FAILED: #{c['case']}")
        end
      end

      private

      def gen_uuid_samples
        [{
          'case' => 'Original file',
          'model_class' => 'event',
          'mounted_as' => 'poster',
          'cache_buster' => 'f14a9b82e5f01a139d7b4b5e756fc779',
          'expected' => 'event-poster-f14a9b82e5f01a139d7b4b5e756fc779'
        },
         {
           'case' => 'Original file',
           'model_class' => 'ckeditor/picture',
           'mounted_as' => 'data',
           'cache_buster' => 'f14a9b82e5f01a139d7b4b5e756fc779',
           'expected' => 'ckeditor/picture-data-f14a9b82e5f01a139d7b4b5e756fc779'
         }]
      end
    end
  end
end
