# frozen_string_literal: true

require 'test/unit'
require 'ostruct'
require_relative '../lib/mapper'

module CarrierWave
  module Storage
    FILE_LABELS = %w[app label].freeze
    FILE_UUID = 'A1234'
    FILE_NAME = 'A1234'
    FILE_CONTENT = 'Data for file'
    FILE_SIZE = 12_000
    FILE_CONTENT_TYPE = 'text/plain'
    FILE_CACHE_CONTROL = 'public, max-age=31536000'
    TRISKEL_PUBLIC_BASE_URL = 'base-url'

    class Client
      def retrieve(_uuid)
        OpenStruct.new(data: FILE_CONTENT)
      end

      def find_by_uuid(_uuid)
        OpenStruct.new(name: FILE_NAME, mime_type: FILE_CONTENT_TYPE, size: FILE_SIZE,
                       cache_control: FILE_CACHE_CONTROL)
      end
    end

    class MapperTest < Test::Unit::TestCase
      def test_map_storage_file_with_uuid
        mapper = CarrierWave::Storage::TriskelFileMapper.new(FILE_LABELS, FILE_CACHE_CONTROL)
        storage_file = mapper.map_storage_file_with_uuid(FILE_UUID, input_file_fixture)
        assert_equal(FILE_UUID, storage_file.uuid)
        assert_equal(FILE_NAME, storage_file.name)
        assert_equal(FILE_CONTENT, storage_file.data)
        assert_equal(FILE_SIZE, storage_file.size)
        assert_equal(FILE_CONTENT_TYPE, storage_file.mime_type)
        assert_equal(FILE_LABELS, storage_file.labels)
        assert_equal(FILE_CACHE_CONTROL, storage_file.cache_control)
      end

      private

      def input_file_fixture
        TriskelSanitizedFile.new(Client.new, FILE_UUID)
      end
    end
  end
end
