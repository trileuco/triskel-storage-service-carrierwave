# frozen_string_literal: true

module CarrierWave
  module Storage
    class TriskelUUIDGenerator
      UUID_DELIMITER = '-'
      def self.gen(model_class, mounted_as, cache_buster)
        [model_class, mounted_as, cache_buster].join(UUID_DELIMITER)
      end
    end
  end
end
