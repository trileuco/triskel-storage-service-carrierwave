# frozen_string_literal: true

require_relative 'triskel_storage_service_ruby_client/lib/triskel_storage_service_ruby_client'
require_relative 'triskel_storage_service_ruby_client/lib/file'

module CarrierWave
  module Storage
    class TriskelSanitizedFile
      def initialize(client, path)
        @client = client
        @path = path
        @file_uuid = ::File.basename(path)
      end

      def file
        filename
      end

      def filename
        @file_uuid
      end

      def original_filename
        load_metadata.name
      end

      def extension
        split_extension(filename)[1] if filename
      end

      def split_extension(filename)
        # regular expressions to try for identifying extensions
        extension_matchers = [
          /\A(.+)\.(tar\.([glx]?z|bz2))\z/, # matches "something.tar.gz"
          /\A(.+)\.([^\.]+)\z/ # matches "something.jpg"
        ]

        extension_matchers.each do |regexp|
          return Regexp.last_match(1), Regexp.last_match(2) if filename =~ regexp
        end
        [filename, ''] # In case we weren't able to split the extension
      end

      def size
        load_metadata.size
      end

      def cache_control
        load_metadata.cache_control
      end

      def labels
        load_metadata.labels
      end

      def path
        "/#{TriskelStorage::HTTPStorage::STORAGE_ENDPOINT}/#{@path}"
      end

      def exists
        response = @client.find_by_uuid(uuid: @file_uuid)
        response.code == 200
      end

      def read
        @read ||= @client.retrieve(@file_uuid).data
      end

      def move_to(_new_path, _permissions = nil, _dir_permissions = nil)
        raise NotImplementedError
      end

      def copy_to(_new_path, _permissions = nil, _dir_permissions = nil)
        raise NotImplementedError
      end

      def to_file
        raise NotImplementedError
      end

      def content_type
        load_metadata.mime_type
      end

      def content_type=(_type)
        raise NotImplementedError
      end

      def delete
        @client.delete_by_uuid(@file_uuid)
      end

      private

      def load_metadata
        @remote_file ||= @client.find_by_uuid(@file_uuid)
      rescue Errno::ECONNREFUSED, Errno::ECONNABORTED, Errno::ECONNRESET
        puts "Cannot retrieve file #{@file_uuid} at storage service due to connection problems"
        fixture_file_for_error(@file_uuid, 'connection_fail.txt')
      rescue TriskelStorage::FileNotFoundError
        puts "Cannot find file #{@file_uuid} at storage service"
        fixture_file_for_error(@file_uuid, 'not_found.txt')
      end

      def fixture_file_for_error(uuid, error_msg)
        file = TriskelStorage::File.new
        file.uuid = uuid
        file.name = error_msg
        file.mime_type = 'application/octet-stream'
        file
      end
    end
  end
end
