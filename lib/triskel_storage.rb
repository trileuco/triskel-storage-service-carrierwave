# frozen_string_literal: true

require_relative 'triskel_storage_service_ruby_client/lib/triskel_storage_service_ruby_client'
require_relative 'mapper'

module CarrierWave
  module Storage
    # Carrierwave storage engine
    class Triskel < Abstract
      def initialize(*)
        super
        @client = TriskelStorage::HTTPStorage.new(@uploader.triskel_base_url)
        @mapper = TriskelFileMapper.new(@uploader.labels, @uploader.cache_control)
      end

      # @param [CarrierWave::SanitizedFile] file
      def store!(file)
        store_path = uploader.store_path
        triskel_file = TriskelSanitizedFile.new(@client, store_path)
        @client.delete_by_uuid(store_path)
        @client.save(@mapper.map_storage_file_with_uuid(store_path, file))
        triskel_file
      end

      # @param [String]
      # @return [TriskelSanitizedFile]
      def retrieve!(uuid)
        TriskelSanitizedFile.new(@client, uploader.store_path(uuid))
      end
    end
  end
end
