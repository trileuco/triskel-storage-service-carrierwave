# frozen_string_literal: true

require 'carrierwave'
require_relative 'triskel_storage'
require_relative 'uuid'

module CarrierWave
  module Uploader
    class Base
      add_config :triskel_base_url
      add_config :cache_control
      add_config :labels

      configure do |config|
        config.storage_engines[:triskel] = 'CarrierWave::Storage::Triskel'
        config.store_dir = nil
        config.cache_control = ''
        config.labels = ['no-custom-labels']
      end
    end
  end
end
