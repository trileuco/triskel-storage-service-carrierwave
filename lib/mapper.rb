# frozen_string_literal: true

require_relative 'file'

module CarrierWave
  module Storage
    class TriskelFileMapper
      def initialize(labels, cache_control)
        @labels = labels
        @cache_control = cache_control
      end

      # @param [CarrierWave::SanitizedFile] input_file
      # @return [TriskelStorage::File]
      def map_storage_file_with_uuid(uuid, input_file)
        file = TriskelStorage::File.new
        file.uuid = uuid
        file.name = input_file.filename
        file.data = input_file.read
        file.size = input_file.size
        file.mime_type = input_file.content_type
        file.cache_control = @cache_control
        file.labels = @labels
        file
      end
    end
  end
end
