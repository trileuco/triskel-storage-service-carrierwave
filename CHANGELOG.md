# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v3.1.1] - 2020-07-03

### Fixed

- [TSSTORAGE-50](https://redmine.trileuco.com/issues/TSSTORAGE-50)
  - Fix warning [DEPRECATION] HTTParty will no longer override response#nil?

## [3.1.0] - 2019-10-07
### Changed
- Updated Ruby version from 2.0.0 to 2.3.1
- Updated carrierwave dependency version range

## [3.0.0] - 2019-10-03
### Added
- Included new class TriskelUUIDGenerator with a method to help to define the unique filename of the uploaded files

### Changed
- BREAKING CHANGE: Removed the `CarrierWave::Uploader::TriskelBaseUploader. Now we can't import it in the clients Uploaders and we don't need any more override the definitions of the methods defined in it.
- BREAKING CHANGE: Base the store and retrieve methods in the `store_path` of carrierwave uploaders instead on the method `storage_uuid` defined by this gem. This method isn't needed any more and now, we will need override in our uploaders the `filename` method as we describe currently in the README (a more carrierwave's standard method).
- The gem is currently registering itself as a Carrierwave's storage engine and declaring its own config parameters, so the clients don't need any more include explicitly the gem in the carrierwave's `config.storage_engines` hash neither define that a `triskel_base_url` config param exists.
- md5 of uploaded files are not sent to Triskel Storage Service any more
- When a model updates some of its attachments, this gem is not deleting explicitly the previous stored files. Now that tasks is delegated in carrierwave gem.

## [2.0.0] - 2019-08-22
### Changed
- Relax httparty and carrierwave gem version restrictions ([#3](https://bitbucket.org/trileuco/triskel-storage-service-carrierwave/pull-requests/3) and [#4](https://bitbucket.org/trileuco/triskel-storage-service-carrierwave/pull-requests/4))
- BREAKING CHANGE: Update the triskel_storage_service_ruby_client version to 1.1.0 and rename this gem from storage_service_carrierwave to triskel_storage_service_carrierwave ([#3](https://bitbucket.org/trileuco/triskel-storage-service-carrierwave/pull-requests/3)). The CarrierWave::Storage::Trileuco storage engine is renamed to CarrierWave::Storage::Triskel. By other hand, the TrileucoBaseUploader is renamed to TriskelBaseUploader and its config key trileuco_base_url to triskel_base_url.  

## [1.1.0] - 2019-07-16
### Added
- Includes md5 sum in headers for file upload to Triskel Storage Service ([#1](https://bitbucket.org/trileuco/triskel-storage-service-carrierwave/pull-requests/1))
- Add CarrierWave::Uploader::TrileucoBaseUploader module to include on Uploaders to add methods that must be implemented. ([#2](https://bitbucket.org/trileuco/triskel-storage-service-carrierwave/pull-requests/2))

### Fixed
- Fix bug when trying to upload empty attachment in form ([#1](https://bitbucket.org/trileuco/triskel-storage-service-carrierwave/pull-requests/1))
- Must remove previous attachment when replacing ([#2](https://bitbucket.org/trileuco/triskel-storage-service-carrierwave/pull-requests/2))

## [1.0.0] - 2019-07-09
### Added
- Initial version of the carrierwave engine for use with the Triskel Storage Service
