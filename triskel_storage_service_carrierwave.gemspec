# frozen_string_literal: true

$:.push File.expand_path('../lib/oauth2/lib', __FILE__)

Gem::Specification.new do |s|
  s.name = 'triskel_storage_service_carrierwave'
  s.version = '3.1.1'
  s.summary = 'Ruby HTTP storage service client adapters related to carrierwave.'
  s.description = 'A gem to adapt storage http client to carrierwave for including in other ruby projects.'
  s.authors = ['trileuco']
  s.email = 'info@trileucosolutions.com'
  s.files = `git ls-files`.split("\n")
  s.homepage = 'https://www.bitbucket.com/trileuco/triskel-storage-service-carrierwave'
  s.add_runtime_dependency 'httparty', ['~> 0.13']
  s.add_runtime_dependency 'carrierwave', ['>= 1.3.1', '< 3.0.0']
  s.add_development_dependency 'rake'
  s.add_development_dependency 'test-unit'
  # get an array of submodule dirs by executing 'pwd' inside each submodule
  gem_dir = File.expand_path(File.dirname(__FILE__)) + "/"
  `git submodule --quiet foreach pwd`.split($\).each do |submodule_path|
    Dir.chdir(submodule_path) do
      submodule_relative_path = submodule_path.sub gem_dir, ""
      # issue git ls-files in submodule's directory and
      # prepend the submodule path to create absolute file paths
      `git ls-files`.split($\).each do |filename|
        s.files << "#{submodule_relative_path}/#{filename}"
      end
    end
  end
end
